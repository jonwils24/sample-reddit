Rails.application.routes.draw do
  root to: redirect('/subs')
  
  resources :users, only: [:new, :create, :show]
  resource :session, only: [:new, :create, :destroy]
  resources :comments, only: [:create]
  resources :subs, except: [:destroy]
  
  resources :posts, except: [:index] do
    resources :comments, only: [:new]
  end
    
  
end