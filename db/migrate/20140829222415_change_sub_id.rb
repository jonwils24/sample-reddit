class ChangeSubId < ActiveRecord::Migration
  def change
    rename_column :posts, :sub_id, :sub_ids
  end
end
