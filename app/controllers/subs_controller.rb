class SubsController < ApplicationController
  
  before_action :require_moderator, only: [:edit]
  
  def new
    if current_user
      @sub = Sub.new
      render :new
    else
      redirect_to new_session_url
    end
  end

  def create
    @sub = Sub.new(sub_params)
    @sub.moderator_id = current_user.id
    
    if @sub.save
      redirect_to sub_url(@sub)
    else
      render :new
    end
  end
  
  def index
    @subs = Sub.all
  end

  def edit
    @sub = Sub.find(params[:id])
    render :edit
  end

  def show
    @sub = Sub.find(params[:id])
    render :show
  end

  def update
    @sub = Sub.find(params[:id])
    
    if @sub.update(sub_params)
      redirect_to sub_url(@sub)
    else
      render :edit
    end
  end
  
  private
  
  def sub_params
    params.require(:sub).permit(:title, :description)
  end
  
  def require_moderator
    unless Sub.find(params[:id]).moderator == current_user
      redirect_to sub_url(params[:id])
    end
  end

end
