class User < ActiveRecord::Base
  validates :username, :password_digest, :session_token, presence: true
  validates :username, :session_token, uniqueness: true

  after_initialize :ensure_session_token
  
  has_many(
    :subs,
    foreign_key: :moderator_id
  )
  
  has_many(
    :post,
    foreign_key: :author_id
    )
    
  has_many(
    :comments,
    foreign_key: :author_id
    )
  
  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end
  
  def self.generate_session_token
    SecureRandom.urlsafe_base64
  end
  
  def is_password?(password)
    BCrypt::Password.new(password_digest) == password
  end 
  
  def reset_session_token!
    self.session_token = User.generate_session_token
    self.save!
    self.session_token
  end
  
  def ensure_session_token
    self.session_token ||= reset_session_token!
  end
  
  def self.find_by_credentials(username, password)
    user = User.find_by_username(username)
    return nil if user.nil?
    user.is_password?(password) ? user : nil
  end
end
