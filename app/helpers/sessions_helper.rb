module SessionsHelper
  
  def is_moderator?(user, sub)
    return nil if user.nil?
    sub.moderator_id == user.id
  end
  
end
