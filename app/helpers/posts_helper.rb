module PostsHelper
  
  def is_author?(user, post)
    return nil if user.nil?
    post.author == user
  end
end
